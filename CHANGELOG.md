
# changelog getXfox

tags utilisés: added  changed  cosmetic  deprecated  fixed  rewriting  removed  syncro  security
date au format: YYYY-MM-DD

## [ Unreleased ]


## [ 4.24.0 ] - 2019.07.08

* fixed: f_wfx_get_version, ver_wfx_online n/a, version suite présence Aurora
* fixed: f_tor_get_version, recup url
* rewriting: f__wget_test
* syncro: fscript_install, fscript_remove, fscript_update, f__user

## [ 4.22.0 ] - 2019.07.04

* fixed: version enligne Tor Browser
* rewriting: récup versions on ligne: f_tor_get_version, f_tor_pers_get_version, f_wfx_get_version, f_wfx_pers_get_version

## [ 4.21.2 ] - 2019.05.4

* fixed: bash v5 utilisable (v4+)

## [ 4.20.0 ] - 2019.04.29

* fixed: changement Url du projet waterfox, correction pour déterminer version disponible et url de téléchargement

## [ 4.19.0 ] - 2018.07.15

* fixed: lanceur /usr/bin, erreur posix, ProfilMangaer inaccessible, f_wfx_install

## [ 4.18.0 ] - 2018.07.03

* synchro: f__requis, f__sort_uniq

## [ 4.17.1 ] - 2018.06.13

* added: f__read_file
* rewriting: suppression sed peu utiles
* rewriting: f__archive_test
* cosmetic: exit sur --help
* cosmetic: shellcheck complet avec nouvelle version (bug sur version antérieure induisait un check incomplet)

## [ 4.17.0 ] - 2018.06.12

* added: option --dev pour chargement version dev
* added: option téléchargement
* syncro: composants, fscript_update avec ses paramètres d'appel
* cosmetic:
* rewriting: traitement options d'appel
* rewriting: f__sudo, plus de multiples tentatives pour su, plus simple et fix potentiel stderr non visible
* rewriting: f_tor_get_version, f_wfx_get_version
* fix: f_wfx_get_version, liens téléchargement et N° version suite changement page du projet


## [ 4.16.2 ] - 2018.06.09

* cosmetic: f__architecture
* fixed: bug mmineur à la mise à jour, si pas de personnalisation installée

## [ 4.16.0 ] - 2018.06.09

* added: f__trim
* rewriting: f__requis (shellcheck)
* cosmetic: affichage help, f_affichage
* syncro: f__color
* fixed: bon renommage fichiers personnalisation waterfox
* fixed: url sur sdeb, supp option test oubliée
* fixed: mise à jour url de personnalisation sur nouveau git
* fixed: f_wfx_lanceur_desktop
* rewriting: f_tor_pers_get_version, f_wfx_pers_get_version, détermination url personnalisée des personnalisations

###  publication sur sdeb/getXfox, historique abandonné

## [ 4.15.0 ] - 2018.03.04

* syncro: f__color, f__info, f__sudo, f__user, f__wget_test
* syncro: fscript_install, fscript_remove, fscript_update
* rewriting: prg_init, f_help
* rewriting: général ubuntu 16.04, f_tor_install, f_wfx_install
* fixed: f_wfx_install, collision architecture si installation groupée wfx & tor

## [ 4.14.2 ] - 2018.02.11

* syncro: f__color

## [ 4.14.1 ] - 2018.01.29

révision: +requis awk>gawk

## [ 4.14.0 ] - 2018.01.26

* rewriting: mineur, fscript_cronAnacron fscript_install fscript_remove fscript_update
* rewriting: f__requis
* fixed: f__sudo, extraction nb tentatives

## [ 4.12.0 ] - 2018.01.24

* rewriting: f_help, f_affichage
* rewriting: f_tor_install, f_wfx_install
* rewriting: général wget_log: fscript_get_version, fscript_update

## [ 4.11.0 ] - 2018.01.16

* added: cumul options (opérations) possibles (sauf opés scripts)
* added: option --sauve pour conserver le téléchargement à l'installation
* rewriting: ffx_lanceur_desktop, mimetype, plus de xul, pas de déclaration images
* rewriting; f__wget_test
* rewriting: f_sudo abandonné dans fscript_install et fscript_remove, au profit appel au traitement général des options
* rewriting: général, invocation f__sudo dans traitement options, plus confortable si su & _all_
* rewriting: f_sudo, format nombre de tentatives et options appel possibles > 1
* rewriting: menu réparation icône pour tous les canaux installés, menu help en conséquence
* rewriting: auto-installation, potentiel bug selon conditions appel
* fixed: test gnome-www-browser avant installation (erreur kde) 

## [ 4.10.0 ] - 2018.01.12

* fixed: correction commentaire fscript_get_version

## [ 4.9.0 ] - 2017.12.29

* syncro: 

## [ 4.8.0 ] - 2017.12.27

* suivi getFirefox

## [ 4.7.0 ] - 2017.12.26

révision: f__info, option combinée raw:log
révision: f_help
révision: deux fonctions config_profil, config_system (traitement profil et défaut système) 
révision: f_wfx_install, lanceur /usr/bin
révision; f_wfx_lanceur_desktop, chemins icone alternatifs
fix: f_wfx_remove, oubli suppression lien dans /usr/bin

## [ 4.5.0 ] - 2017.12.25

* cosmetic: 
* rewriting: tor_install, wfx_install, wfx_profil_user
* rewriting: général, plus de variables composées avec variable dans local


## [ 4.3.0 ] - 2017.12.24

* cosmetic: 
* fixed: f__wget_test, incompatible avec redirection logs
* fixed: typo fscript_update

## [ 4.1.0 ] - 2017.12.23

* base getFirefox 4.1.0
* prise en charge waterfox et tor browser

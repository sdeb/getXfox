/*v0.2*/

// par défaut:
user_pref("intl.accept_languages", "fr, fr-fr, en-us, en");
user_pref("general.useragent.locale", "fr");

// ne prévient pas de la fermeture d'onglets
user_pref("browser.tabs.warnOnClose", false);

// pas d'avertissement sur ouverture plusieurs onglets
user_pref("browser.tabs.warnOnOpen", false);

// pas d'avertissement sur about:config
user_pref("general.warnOnAboutConfig", false);

// pas de fermeture fenêtre lors fermeture dernier onglet
user_pref("browser.tabs.closeWindowWithLastTab", false);

//nb d'onglet pouvant être réouverts, standard 10
user_pref("browser.sessionstore.max_tabs_undo", 10);
//nb de fenêtres pouvant être réouvertes, standard 3
user_pref("browser.sessionstore.max_windows_undo", 10);

//pas de newtabpage (page blanche) mais toujours saloperie engrenage pour activer facilement :(
user_pref("browser.newtabpage.enabled", false);
// pas de sites suggérés
user_pref("browser.newtabpage.enhanced", false);
// sources des sites suggérés 
//ini: user_pref("browser.newtabpage.directory.source", "https://tiles.services.mozilla.com/v3/links/fetch/%LOCALE%/%CHANNEL%");
user_pref("browser.newtabpage.directory.source", "");
user_pref("browser.newtab.preload", false);

//stop démarrage auto des lecteurs media, peut-être limité aux vidéos HTML5
user_pref("media.autoplay.enabled", false);

// liens visités, https://bugzilla.mozilla.org/show_bug.cgi?id=385111, http://htmlcolorcodes.com/fr/
// ne force pas sur les choix déplorables en terme de contraste de certains sites
// std: user_pref("browser.visited_color", "#551A8B"); bleu foncé
// clair: pref("browser.visited_color", "#CC33CC"); Medium Faded Magenta
// clair: pref("browser.visited_color", "#CC00CC"); Dark Hard Magenta
// purple (foncé): pref("browser.visited_color", "#800080");
// Dark Dull Magenta, plus clair: 
user_pref("browser.visited_color", "#993399");

//force punycode pour idn anti_phishing, non résolu depuis début 2017!
user_pref("network.IDN_show_punycode", true);

// pas de masquage de http:// (https:// pas concerné) et du slash final
user_pref("browser.urlbar.trimURLs", false);

//si true, sélectionne tout sur un clic dans la barre d'adresse 
user_pref("browser.urlbar.clickSelectsAll", true);

// désactiver la manipulation du click droit/menu contextuel par js
user_pref("dom.event.contextmenu.enabled", false);
// désactiver la manipulation du presse papier, true par défaut, mais possible pbs sécu/privacy? par js
user_pref("dom.event.clipboardevents.enabled", false);
// désactiver resize et move par js 
user_pref("dom.disable_window_move_resize", true);
// désactiver mise en avant ou en background par js 
user_pref("dom.disable_window_flip", true);
// désactiver voulez vous vraiment quitter la page par javascript
user_pref("dom.disable_beforeunload", true);
// désactiver la désactivation du bouton close sur une popup par javascript
user_pref("dom.disable_window_open_feature.close", true);
// désactiver la désactivation barre adresse sur une popup par javascript
user_pref("dom.disable_window_open_feature.location", true);
// désactiver la désactivation barre menu sur une popup par javascript
user_pref("dom.disable_window_open_feature.menubar", true);
// désactiver la désactivation minimisable sur une popup par javascript
user_pref("dom.disable_window_open_feature.minimizable", true);
// désactiver la désactivation de la barre personelle sur une popup par javascript
user_pref("dom.disable_window_open_feature.personalbar", true);
// désactiver la désactivation du redimensionnement sur une popup par javascript
user_pref("dom.disable_window_open_feature.resizable", true);
// désactiver la désactivation des ascenseurs sur une popup par javascript, encore valable?
user_pref("dom.disable_window_open_feature.scrollbars", true);
// désactiver la désactivation barre de statut sur une popup par javascript, encore valable?
user_pref("dom.disable_window_open_feature.status", true);
// désactiver la désactivation barre de titre sur une popup par javascript, encore valable?
user_pref("dom.disable_window_open_feature.titlebar", true);
// désactiver la désactivation barre d'outils sur une popup par javascript, encore valable?
user_pref("dom.disable_window_open_feature.toolbar", true);
// désactiver le changement de statut par javascript, encore valable?
user_pref("dom.disable_window_status_change", true);
